
function AdRecall(resources)
{
	AdRecall.resources = resources;
}
AdRecall.prototype = {
	init: function()
	{
		this.game = new Phaser.Game(800, 298, Phaser.CANVAS, 'AdRecall', { preload: this.preload, create: this.create, update: this.update, render: 
		this.render,parent:this });
	},

	preload: function()
	{

        this.game.scale.maxWidth = 800;
    	this.game.scale.maxHeight = 600;
		this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		this.game.load.image('bckg', AdRecall.resources.bckg);
		this.game.load.image('brain', AdRecall.resources.brain);
    	this.game.load.image('c_r', AdRecall.resources.c_r);
		this.game.load.image('u_l', AdRecall.resources.u_l);
		this.game.load.image('c_l', AdRecall.resources.c_l);
		this.game.load.image('l_l', AdRecall.resources.l_l);
		//Phaser.ScaleManager.compatibility.scrollTo=false;

	},

	create: function(evt)
	{
     
    //Call the animation build function
    this.parent.buildAnimation();

	},

	cycleData: function(evt){

	},

	up: function(evt)
			{
			//	debugger
		    //console.log('button up', arguments);
	}
		,
	over: function(evt)
			{
		    //console.log('button over');
	}
		,
	out: function(evt)
			{
		    //console.log('button out');
	}
	,

	buildAnimation: function()
	{


    //Background
    this.bckg = this.game.add.sprite(0, 0, 'bckg');
   //this.bckg.anchor.set(0.5);
    
    //STYLE
    var style = {font:"bold 20px freight-sans-pro", fill: "#000000", wordWrap: false,wordWrapWidth: 400, align: "left"};

    var styleFunnel = {font:"bold 12px freight-sans-pro", fill: "#000000", wordWrap: true, wordWrapWidth: 300, align: "Center",lineSpacing: -8 };
        
  var styleShort = {font:"bold 12px freight-sans-pro", fill: "#000000", wordWrap: true, wordWrapWidth: 100, align: "Center",lineSpacing: -8 };
        

      //800x x 600y

        this.u_l = this.game.add.sprite(-500, 0, 'u_l');

        this.c_l = this.game.add.sprite(-500, 100, 'c_l');
       
        this.l_l = this.game.add.sprite(-500, 190, 'l_l');
       

		this.c_r = this.game.add.sprite(1690, 0, 'c_r');

		this.brain = this.game.add.sprite(1460, 0, 'brain');
		//this.brain.alpha = 0;

		this.pfield_0 = this.game.add.text(this.game.world.centerX - 80, this.game.world.centerY + 100, AdRecall.resources.ico0text, styleFunnel);

        this.pfield_1 = this.game.add.text(100, 80, AdRecall.resources.ico1text, styleFunnel);
		this.pfield_2 = this.game.add.text(5, 160, AdRecall.resources.ico2text, styleFunnel);
		this.pfield_3 = this.game.add.text(100, 250, AdRecall.resources.ico3text, styleFunnel);
		this.pfield_4 = this.game.add.text(500, 170, AdRecall.resources.ico4text, styleFunnel);
		this.pfield_5 = this.game.add.text(690, 170, AdRecall.resources.ico5text, styleShort);

		this.pfield_1.alpha = 0;
		this.pfield_2.alpha = 0;
		this.pfield_3.alpha = 0;
		this.pfield_4.alpha = 0;
		this.pfield_5.alpha = 0;

this.tweenu_l = this.game.add.tween(this.u_l).to( { x: 0 }, 1000, Phaser.Easing.Linear.Out, true, 100);

	this.tweenl_1Text = this.game.add.tween(this.pfield_1).to( { alpha: 1 }, 1500, Phaser.Easing.Linear.Out, true, 1000);

this.tweenc_l = this.game.add.tween(this.c_l).to( { x: 0 }, 1000, Phaser.Easing.Linear.Out, true, 1500);


	this.tweenl_2Text = this.game.add.tween(this.pfield_2).to( { alpha: 1 }, 1500, Phaser.Easing.Linear.Out, true, 2000);

this.tweenl_l = this.game.add.tween(this.l_l).to( { x: 0 }, 1000, Phaser.Easing.Linear.Out, true, 2500);

	this.tweenl_3Text = this.game.add.tween(this.pfield_3).to( { alpha: 1 }, 1500, Phaser.Easing.Linear.Out, true, 3000);

this.tweenCenter = this.game.add.tween(this.brain).to( { x: 460 }, 1000, Phaser.Easing.Linear.Out, true,3500);


	this.tweenl_4Text = this.game.add.tween(this.pfield_4).to( { alpha: 1 }, 1500, Phaser.Easing.Linear.Out, true, 4000);

this.tweenc_r = this.game.add.tween(this.c_r).to( { x: 690 }, 1000, Phaser.Easing.Linear.Out, true, 4500);

	this.tweenl_5Text = this.game.add.tween(this.pfield_5).to( { alpha: 1 }, 1500, Phaser.Easing.Linear.Out, true, 5000);






	///this.tweenu_lText = this.game.add.tween(this.pfield_2).to( { x: 30 }, 1500, Phaser.Easing.Linear.Out, true, 2000);
	//this.tweenc_rText = this.game.add.tween(this.pfield_3).to( { x: 600 }, 1500, Phaser.Easing.Linear.Out, true, 3000);
			

	},


	actionOnClick: function(evt){

	},

	animate: function()
	{

	},

/////////////////////////////////////////////////

	update: function()
	{

	},

	render: function()
	{

	}

}