
function Chart8(resources)
{
  Chart8.resources = resources;
}
Chart8.prototype = {
  init: function()
  {


    /// background-image:url('course/en/images/chartback/chartback1.jpg');

///SET CHART TYPE FROM JSON  
var barLine = Chartist.Line

var fnstring = Chart8.resources.chart_style

//LOAD AND CREATE ARRAYS
var labelArray = Chart8.resources.nodeText;
var nodeArray = new Array();
var seriesNodes = new Array();
var nodeArray = new Array();

nodeArrayString = Chart8.resources._nodes[0].nodeNum;


//var original = [1,2,3];        //Define original array
//var copy = original.slice(0);

var clonedArr = nodeArrayString.slice(0);

var convArr = $.makeArray(clonedArr);

console.log("convArr"+convArr[3]);
var len2 = convArr.length;


console.log("len2 = " + len2 );


var len = Chart8.resources._nodes.length;

///LOOP THRU DATA
for(var i = 0; i<len; i++) {
nodeArray.push(Chart8.resources._nodes[i].nodeNum);
}

//CREATE SERIES ARRAY OF NODES
for(var n = 0; n<len; n++) {
 //var name = seriesNames[n]

 var name = "series" + (n+1)
 var data = nodeArray[n]
 seriesNodes.push({name,data});
}

//Dotted chart class concat
var _dotchartclass =  Chart8.resources._nodotchartclass;
var _dotdotchartclass = String("."+_dotchartclass)
console.log("_dotchartclass = " + _dotdotchartclass);

  ///Booleans
  var stack_Bars = Boolean(Chart8.resources.stack_bars);
  var reverse_Data  = Boolean(Chart8.resources.reverse_data);
  var horizontal_Bars = Boolean(Chart8.resources.horizontal_bars);
  var _showLabel = Boolean(Chart8.resources.showLabel);

  var _donut = Boolean(Chart8.resources.pieDonut);
  var show_area  =  Boolean(Chart8.resources.show_area);
  var show_point = Boolean(Chart8.resources.show_point);
  var animate = Boolean(Chart8.resources.animate_num);

  var  _startAngle = Chart8.resources.pieStartAngle;
  var _total = Chart8.resources.pieTotal;

  //SIZES AND POSITIONS
  var x_pos = Chart8.resources.x_pos;
  var y_pos = Chart8.resources.y_pos;
  var chart_width = Chart8.resources.chart_width;
  var chart_height = Chart8.resources.chart_height;

  var _low =   Chart8.resources.lowestNum;
  var _high =   Chart8.resources.highestNum;


//SMOOTHING
var line_smooth_switch = String(Chart8.resources.line_smooth);
var line_smooth = Chartist.Interpolation.cardinal();

switch (line_smooth_switch) {
case "step": 
line_smooth = Chartist.Interpolation.step();
break;
case "simple": 
line_smooth = Chartist.Interpolation.simple();
break;
case "none": 
line_smooth = Chartist.Interpolation.none();
break;
case "normal": 
line_smooth = Chartist.Interpolation.cardinal();
break;
}

/// CHART TYPE

switch (fnstring) {
case "line": 
barLine = Chartist.Line
break;
case "bar": 
barLine = Chartist.Bar
break;
case "pie": 
barLine = Chartist.Pie
break;
}

///



if(fnstring == "line" || fnstring == "bar"){

var ctline = new barLine(_dotdotchartclass, {
  labels: labelArray,
  series: seriesNodes
}, {
  

  ///fullWidth: true,
  // Within the series options you can use the series names
  // to specify configuration that will only be used for the
  // specific series.
  
  width: chart_width,
  height: chart_height,

  low: _low,
  high: _high,
  
  stackBars: stack_Bars,
  reverseData: reverse_Data,
  horizontalBars: horizontal_Bars,
  showArea: show_area,
  showPoint: show_point,
  lineSmooth: line_smooth,
  //chartPadding: 30,
  labelOffset: 100,
    //labelDirection: 'explode',
    //labelInterpolationFnc: function(value) {
     // return value;
    //},
  ///seriesBarDistance: 5,

 // scaleMinSpace: 20,
  // Can be set to true or false. If set to true, the scale will be generated with whole numbers only.
  //onlyInteger: true,
  // The reference value can be used to make sure that this value will always be on the chart. This is especially useful on bipolar charts where the bipolar center always needs to be part of the chart.
  axisX: {
    // On the x-axis start means top and end means bottom
    position: x_pos
  },
  axisY: {
    // On the y-axis start means left and end means right
    position: y_pos,
    labelInterpolationFnc: function(value) {
      return  ''
    },
    scaleMinSpace: 30
  },
  series: {
    'series1': {
      lineSmooth: Chartist.Interpolation.none(),
      showPoint: false,
      showArea: true
    },
    'series2': {
      showPoint: true
    },
    'series3': {
      showPoint: true
    }
  }

},[
  ['screen and (max-width: 600px)', {

  width: 300,
  height: 300,
  lineSmooth: true,
  horizontalBars: false,
  showArea: false,
  seriesBarDistance: 5,

  series: {
    'series1': {
      lineSmooth: Chartist.Interpolation.none(),
      showPoint: false
    },
    'series2': {
       lineSmooth: Chartist.Interpolation.none(),
      showPoint: false
    },
    'series3': {
      lineSmooth: Chartist.Interpolation.none(),
      showPoint: false
    }
  }

  }]
]);

if(fnstring == "pie"){

var ctline = new barLine(_dotdotchartclass, {

  labels: labelArray,
  series: seriesNodes[0]

  }, {
    chartPadding: 10,
    labelOffset: 10,
    labelDirection: 'explode',
    
    width: chart_width,
    height: chart_height,

      low: _low,
      high: _high,

    donut: _donut,
    startAngle: _startAngle,
    total: _total,
    showLabel: _showLabel
    
    },[
    ['screen and (max-width: 500px)', {
    width: 400,
    height: 300
    }]
  ]);

}



ctline.on('draw', function(data) {
  // If the draw event was triggered from drawing a point on the line chart
  if(data.type === 'point') {
    // We are creating a new path SVG element that draws a triangle around the point coordinates
    var triangle = new Chartist.Svg('path', {
      d: [
        'M',
        data.x - 50,
        data.y - 30,
        'L',
        data.x + 50,
        data.y - 30,
        'L',
        data.x + 50,
        data.y - 10,
        'L',
        data.x + 5,
        data.y - 10,
        'L',
        data.x,
        data.y + 0,
        'L',
        data.x - 5,
        data.y - 10,
        'L',
        data.x - 50,
        data.y - 10,
        'z'].join(' '),
      style: 'fill-opacity: 1'
    }, 'ct-area');

    // With data.element we get the Chartist SVG wrapper and we can replace the original point drawn by Chartist with our newly created triangle
    data.element.replace(triangle);
  }
});


if(animate == 1){
// Let's put a sequence number aside so we can use it in the event callbacks
var seq = 0,
  delays = 80,
  durations = 500;

// Once the chart is fully created we reset the sequence
ctline.on('created', function() {
  seq = 0;
});

// On each drawn element by ctlineist we use the ctlineist.Svg API to trigger SMIL animations
ctline.on('draw', function(data) {
  seq++;

  if(data.type === 'line') {
    // If the drawn element is a line we do a simple opacity fade in. This could also be achieved using CSS3 animations.
    data.element.animate({
      opacity: {
        // The delay when we like to start the animation
        begin: seq * delays + 1000,
        // Duration of the animation
        dur: durations,
        // The value where the animation should start
        from: 0,
        // The value where it should end
        to: 1
      }
    });
  } else if(data.type === 'label' && data.axis === 'x') {
    data.element.animate({
      y: {
        begin: seq * delays,
        dur: durations,
        from: data.y + 100,
        to: data.y,
        // We can specify an easing function from ctlineist.Svg.Easing
        easing: 'easeOutQuart'
      }
    });
  } else if(data.type === 'label' && data.axis === 'y') {
    data.element.animate({
      x: {
        begin: seq * delays,
        dur: durations,
        from: data.x - 100,
        to: data.x,
        easing: 'easeOutQuart'
      }
    });
  } else if(data.type === 'point') {
    data.element.animate({
      x1: {
        begin: seq * delays,
        dur: durations,
        from: data.x - 10,
        to: data.x,
        easing: 'easeOutQuart'
      },
      x2: {
        begin: seq * delays,
        dur: durations,
        from: data.x - 10,
        to: data.x,
        easing: 'easeOutQuart'
      },
      opacity: {
        begin: seq * delays,
        dur: durations,
        from: 0,
        to: 1,
        easing: 'easeOutQuart'
      }
    });
  } else if(data.type === 'grid') {
    // Using data.axis we get x or y which we can use to construct our animation definition objects
    var pos1Animation = {
      begin: seq * delays,
      dur: durations,
      from: data[data.axis.units.pos + '1'] - 30,
      to: data[data.axis.units.pos + '1'],
      easing: 'easeOutQuart'
    };

    var pos2Animation = {
      begin: seq * delays,
      dur: durations,
      from: data[data.axis.units.pos + '2'] - 100,
      to: data[data.axis.units.pos + '2'],
      easing: 'easeOutQuart'
    };

    var animations = {};
    animations[data.axis.units.pos + '1'] = pos1Animation;
    animations[data.axis.units.pos + '2'] = pos2Animation;
    animations['opacity'] = {
      begin: seq * delays,
      dur: durations,
      from: 0,
      to: 1,
      easing: 'easeOutQuart'
    };

    data.element.animate(animations);
  }
});

// For the sake of the example we update the ctline every time it's created with a delay of 10 seconds
ctline.on('created', function() {
  if(window.__exampleAnimateTimeout) {
    clearTimeout(window.__exampleAnimateTimeout);
    window.__exampleAnimateTimeout = null;
  }
  window.__exampleAnimateTimeout = setTimeout(ctline.update.bind(ctline), 12000);
});
 
 }

}

//this.addFloatLabel();
},

/*
addFloatLabel: function() {

//Create an input type dynamically.
var element = document.createElement("input");

element.setAttribute("style", "width:200px");

//Create Labels
var label = document.createElement("Label");
label.innerHTML = "New Label";     

//Assign different attributes to the element.
element.setAttribute("type", "text");
element.setAttribute("value", "");
element.setAttribute("name", "Test Name");
element.setAttribute("style", "width:200px");

this.$('.*')

label.setAttribute("style", "font-weight:normal");

this.appendChild(label);
this.appendChild(element);

element.x = 100;
element.y = 100;

// 'foobar' is the div id, where new fields are to be added
//var foo = document.getElementById("fooBar");

//Append the element in page (in span).
//foo.appendChild(label);
//foo.appendChild(element);
}
,
*/

preload: function()
  {


  },

  create: function(evt)
  {
    //Call the animation build function
    this.parent.buildAnimation();
  },

     update: function()
  {

  }
}